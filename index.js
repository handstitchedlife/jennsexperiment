let blueBox = document.querySelector("div.blueBox");
blueBox.addEventListener("click", () => {
    blueBox.style.background = blueBox.style.background === "blue" ? "red" : "blue";
});
